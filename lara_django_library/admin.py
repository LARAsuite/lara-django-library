# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Library, LibItemCreatorType, LibItemCreator, LibItemAnnotation, LibItemNote, LibItemDataType, LibItemData, LibItemType, LibItem, LibItemCollection, FulltextLibItemWords, FulltextLibItem, FeedItem, Feed


@admin.register(Library)
class LibraryAdmin(admin.ModelAdmin):
    list_display = ('library_id', 'name', 'description')
    search_fields = ('name',)


@admin.register(LibItemCreatorType)
class LibItemCreatorTypeAdmin(admin.ModelAdmin):
    list_display = ('libitem_creator_type_id', 'creator_type', 'description')


@admin.register(LibItemCreator)
class LibItemCreatorAdmin(admin.ModelAdmin):
    list_display = ('libitem_creator_id', 'entity', 'creator_type')
    list_filter = ('entity', 'creator_type')


@admin.register(LibItemAnnotation)
class LibItemAnnotationAdmin(admin.ModelAdmin):
    list_display = (
        'libitem_annotation_id',
        'caption',
        'entity',
        'parent',
        'textNode',
        'offset',
        'x',
        'y',
        'cols',
        'rows',
        'text',
        'collapsed',
        'date_creation',
        'date_modified',
        'synced',
    )
    list_filter = (
        'entity',
        'collapsed',
        'date_creation',
        'date_modified',
        'synced',
    )


@admin.register(LibItemNote)
class LibItemNoteAdmin(admin.ModelAdmin):
    list_display = (
        'libitem_note_id',
        'title',
        'note',
        'date_creation',
        'date_modified',
        'synced',
    )
    list_filter = ('date_creation', 'date_modified', 'synced')


@admin.register(LibItemDataType)
class LibItemDataTypeAdmin(admin.ModelAdmin):
    list_display = ('libitem_type_id', 'item_type', 'description')


@admin.register(LibItemData)
class LibItemDataAdmin(admin.ModelAdmin):
    list_display = (
        'libitem_data_id',
        'name',
        'data_type',
        'data_text',
        'data_bin',
        'media_type',
        'data_file',
        'data_image',
        'URL',
        'annotation',
        'description',
    )
    list_filter = ('data_type', 'media_type', 'annotation')
    raw_id_fields = ('tags',)
    search_fields = ('name',)


@admin.register(LibItemType)
class LibItemTypeAdmin(admin.ModelAdmin):
    list_display = ('libitem_type_id', 'item_type', 'description')


@admin.register(LibItem)
class LibItemAdmin(admin.ModelAdmin):
    list_display = (
        'libitem_id',
        'libitem_type',
        'library',
        'name',
        'acronym',
        'acronyms',
        'caption',
        'text',
        'UUID',
        'IRI',
        'handle',
        'hash_SHA256',
        'URL',
        'DOI',
        'bibTeX',
        'date_added',
        'date_modified',
        'client_date_modified',
        'version',
        'synced',
        'key',
        'description',
    )
    list_filter = (
        'libitem_type',
        'library',
        'date_added',
        'date_modified',
        'client_date_modified',
        'synced',
    )
    raw_id_fields = ('creators', 'data', 'see_also', 'tags')
    search_fields = ('name',)


@admin.register(LibItemCollection)
class LibItemCollectionAdmin(admin.ModelAdmin):
    list_display = (
        'libitem_collection_id',
        'name',
        'date_added',
        'date_modified',
        'client_date_modified',
        'key',
    )
    list_filter = ('date_added', 'date_modified', 'client_date_modified')
    raw_id_fields = ('libitems',)
    search_fields = ('name',)


@admin.register(FulltextLibItemWords)
class FulltextLibItemWordsAdmin(admin.ModelAdmin):
    list_display = ('word_id', 'word')


@admin.register(FulltextLibItem)
class FulltextLibItemAdmin(admin.ModelAdmin):
    list_display = (
        'fulltextitem_id',
        'libitem',
        'indexed_pages',
        'total_pages',
        'indexed_chars',
        'total_chars',
        'version',
        'synced',
        'date_added',
        'date_modified',
        'client_date_modified',
    )
    list_filter = (
        'libitem',
        'synced',
        'date_added',
        'date_modified',
        'client_date_modified',
    )
    raw_id_fields = ('words',)


@admin.register(FeedItem)
class FeedItemAdmin(admin.ModelAdmin):
    list_display = ('feeditem_id', 'caption', 'text')


@admin.register(Feed)
class FeedAdmin(admin.ModelAdmin):
    list_display = (
        'feed_id',
        'name',
        'URL',
        'last_update',
        'last_check',
        'lastCheckError',
        'cleanupReadAfter',
        'cleanupUnreadAfter',
        'refreshInterval',
    )
    list_filter = ('last_update', 'last_check')
    search_fields = ('name',)
