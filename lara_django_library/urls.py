"""_____________________________________________________________________

:PROJECT: LARA

*lara_library urls *

:details: lara_library urls module.
         - add app specific urls here

:file:    urls.py
:authors:  mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.2.33"

from django.urls import path, include
from django.views.generic import TemplateView

# Add your lara_library urls here.
app_name = 'lara_django_library'

urlpatterns = [
    # path('', TemplateView.as_view(template_name='index.html'), name='lara_library-main-view'),
    # path('subdir/', include('.urls')),
]
