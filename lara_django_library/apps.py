"""_____________________________________________________________________

:PROJECT: LARA

*lara_django_library apps *

:details: lara_library app configuration. This provides a genaric 
         django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/2.0/ref/applications/

:file:    apps.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.2.33"

from django.apps import AppConfig


class LaraLibraryConfig(AppConfig):
    name = 'lara_django_library'
    # verbose_name = "enter a verbose name for your app: lara_library here - this will be used in the admin interface"
    # lara_app_icon = 'lara_library_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.

    # def ready(self):
    #     from django.urls import path, include
    #     from lara_django_base.urls import urlpatterns

    #     urlpatterns.append(
    #         path('library/', include('lara_django_library.urls')))
