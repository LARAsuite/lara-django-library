"""_____________________________________________________________________

:PROJECT: LARA

*lara_library models *

:details: lara_library database models. 
         - this model is inspired by
           the Zotero 5 database model 
           to retain compatibility/convertibility:
           It tried to keep as many relations as possible from Zotero, 
           but using LARA relations and tables
           
           charsets: all text in this database shall be UTF-8 encoded

:file:    models.py
:authors: 

:date: (creation)          20190618
:date: (last modification) 20190703

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.2.33"

import uuid

from django.utils import timezone

from django.db import models

from lara_django_base.models import MediaType, Tag
from lara_django_people.models import Entity


class Library(models.Model):
    """Library = collection of literature, media files and references """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    library_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="name of the library")
    description = models.TextField(
        blank=True, null=True, help_text="description of the library")

    def __str__(self):
        return self.description  # f"{self.name} ({self.description})" or ""

    def __repr__(self):
        return self.description  # f"{self.name} ({self.description})" or ""


class LibItemCreatorType(models.Model):
    """Type of Lib Item Creator: 
       e.g. author, editor, camera man,  
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libitem_creator_type_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    creator_type = models.TextField(
        unique=True, null=True, help_text="name of creator type")
    description = models.TextField(
        blank=True, null=True, help_text="description of extra data type")

    def __str__(self):
        return self.creator_type or ""

    def __repr__(self):
        return self.creator_type or ""


class LibItemCreator(models.Model):
    """Lib Item Creator: 
       combination of type (author, editor, camera man) and Entity
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libitem_creator_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    entity = models.ForeignKey(Entity, related_name='%(app_label)s_%(class)s_entities_related',
                               related_query_name="%(app_label)s_%(class)s_entities_related_query",
                               on_delete=models.CASCADE, null=True, blank=True, help_text="name of creator type")
    creator_type = models.ForeignKey(LibItemCreatorType,  related_name='%(app_label)s_%(class)s_creator_type_related',
                                     related_query_name="%(app_label)s_%(class)s_creator_type_related_query",
                                     on_delete=models.CASCADE, null=True, blank=True, help_text="name of creator type")

    def __str__(self):
        return self.entity.name_full or ""

    def __repr__(self):
        return self.entity.name_full or ""


class LibItemAnnotation(models.Model):
    """ annotations to lib items, e.g. PDF annotations """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libitem_annotation_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    caption = models.TextField(
        blank=True, null=True, help_text="description of extra data type")
    text = models.TextField(blank=True, null=True,
                            help_text="description of extra data type")
    entity = models.ForeignKey(Entity, related_name='%(app_label)s_%(class)s_entities_related',
                               related_query_name="%(app_label)s_%(class)s_entities_related_query",
                               on_delete=models.CASCADE, null=True, blank=True, help_text="entity that created annotation")
    # itemID - back reference to item
    parent = models.TextField(blank=True, null=True,
                              help_text="description of extra data type")
    textNode = models.IntegerField(blank=True, null=True, help_text="")
    offset = models.IntegerField(blank=True, null=True, help_text="")
    x = models.IntegerField(blank=True, null=True, help_text="")
    y = models.IntegerField(blank=True, null=True, help_text="")
    cols = models.IntegerField(blank=True, null=True, help_text="")
    rows = models.IntegerField(blank=True, null=True, help_text="")
    text = models.TextField(blank=True, null=True,
                            help_text="description of extra data type")
    collapsed = models.BooleanField(default=False)
    date_creation = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    date_modified = models.DateTimeField(
        blank=True, null=True, help_text=" default: date time of now")
    synced = models.BooleanField(default=False)

    # FOREIGN KEY (itemID) REFERENCES itemAttachments(itemID) ON DELETE CASCADE

    def __str__(self):
        return self.caption or ""

    def __repr__(self):
        return self.caption or ""


class LibItemNote(models.Model):
    """ A generic Reference """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libitem_note_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    title = models.TextField(blank=True, null=True,
                             help_text="title of item note")
    note = models.TextField(blank=True, null=True, help_text="item note")
    date_creation = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    date_modified = models.DateTimeField(
        blank=True, null=True, help_text=" default: date time of now")
    synced = models.BooleanField(default=False)

    def __str__(self):
        return self.title or ""

    def __repr__(self):
        return self.title or ""


class LibItemDataType(models.Model):
    """ item class or types like, e.g.,
        reference, literature, method (class defines type JSON/XML/TXT)
        migt be replaced by MediaType
    """
    libitem_type_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    item_type = models.TextField(
        unique=True, help_text="name of the item data type/class, like 'journal_article/title', 'journal_article/publishing_date',... ")
    description = models.TextField(
        blank=True, help_text="description of the itemclass")

    def __str__(self):
        return self.item_type or ""  # description

    def __repr__(self):
        return self.item_type or ""


class LibItemData(models.Model):
    """ A generic libitem data
        e.g. journal name, book title, publishing year, ISBN
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libitem_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="name of the data")
    data_type = models.ForeignKey(LibItemDataType, related_name='%(app_label)s_%(class)s_data_types_related',
                                  related_query_name="%(app_label)s_%(class)s_data_types_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="defines, what kind of data is stored, like 'supplemental information' ")
    data_text = models.TextField(
        blank=True, null=True, help_text="generic xml/svg/xhtml field")
    data_bin = models.BinaryField(
        blank=True, null=True, help_text="generic binary field")
    media_type = models.ForeignKey(MediaType, related_name="%(app_label)s_%(class)s_media_types_related",
                                   on_delete=models.CASCADE, null=True, blank=True,  help_text="file type of extra data file")
    data_file = models.FileField(
        upload_to='library/itemdata/', blank=True, null=True, help_text="rel. path/filename")
    data_image = models.ImageField(upload_to='library/itemdata/images/', blank=True, default="image.svg",
                                   help_text="location room map rel. path/filename to image")
    URL = models.URLField(
        blank=True, help_text="Universal Resource Locator - URL")
    tags = models.ManyToManyField(Tag,  blank=True, related_name='%(app_label)s_%(class)s_tags_related',
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query",
                                  help_text="tags")
    annotation = models.ForeignKey(LibItemAnnotation, related_name='%(app_label)s_%(class)s_annotations_related',
                                   related_query_name="%(app_label)s_%(class)s_annotations_related_query",
                                   on_delete=models.CASCADE, null=True, blank=True, help_text="lib item annotations")
    description = models.TextField(
        blank=True, null=True, help_text="description of item data")

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""

    class Meta:
        verbose_name_plural = 'LibItemData'


class LibItemType(models.Model):
    """ item class or types like, e.g.,
        reference, literature, method (class defines type JSON/XML/TXT)
        migt be replaced by MediaType
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libitem_type_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    item_type = models.TextField(
        unique=True, help_text="name of the type/class, like 'reference'")
    description = models.TextField(
        blank=True, help_text="description of the itemclass")

    def __str__(self):
        return self.item_type or ""  # description

    def __repr__(self):
        return self.item_type or ""


class LibItem(models.Model):
    """ A generic Reference 
        can be used for modelling a medium, e.g. a journal or book
        or an part of a medium, like an article or chapter
    """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libitem_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    libitem_type = models.ForeignKey(LibItemType, related_name='%(app_label)s_%(class)s_libitem_type_related',
                                     related_query_name="%(app_label)s_%(class)s_libitem_related_query",
                                     on_delete=models.CASCADE, null=True, blank=True)
    library = models.ForeignKey(Library, related_name='%(app_label)s_%(class)s_libraries_related',
                                related_query_name="%(app_label)s_%(class)s_libraries_related_query",
                                on_delete=models.CASCADE, null=True, blank=True, help_text="reference to library")
    name = models.TextField(blank=True, null=True, help_text="name caption")
    acronym = models.TextField(
        blank=True, null=True, help_text="most common acronym")
    # -> JSON
    acronyms = models.TextField(
        blank=True, null=True, help_text="all known acronyms")
    caption = models.TextField(blank=True, null=True, help_text="item caption")
    text = models.TextField(blank=True, null=True, help_text="item text")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="UUID of the item")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    handle = models.URLField(
        blank=True, null=True,  unique=True, help_text=" [handle URI](https://www.handle.net/)")
    creators = models.ManyToManyField(LibItemCreator, related_name='%(app_label)s_%(class)s_creators_related',
                                      related_query_name="%(app_label)s_%(class)s_creators_related_query",
                                      blank=True, help_text="item creators with roles: author, editor, publisher, ...")
    data = models.ManyToManyField(LibItemData, related_name="%(app_label)s_%(class)s_data_related",
                                  blank=True, help_text="data associated with the libitem, like attachements, ...")
    hash_SHA256 = models.CharField(max_length=256, blank=True, null=True,
                                   help_text="SHA256 hash of item for identity/dublicate checking")
    URL = models.URLField(
        blank=True, help_text="Universal Resource Locator - URL")
    DOI = models.URLField(
        blank=True, help_text="Digital Object Identifier - DOI")
    bibTeX = models.TextField(blank=True, help_text="bibTeX reference entry")
    # ~ reference = models.TextField(blank=True, help_text="bibliographic reference in a format specified by the data")
    see_also = models.ManyToManyField('self',
                                      blank=True, help_text="reference to other items")
    date_added = models.DateTimeField(
        default=timezone.now, help_text="date when item was inserted into database -  default: date time of now")
    date_modified = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    client_date_modified = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    version = models.IntegerField(
        blank=True, null=True, help_text="version of ??")
    synced = models.BooleanField(default=False)
    key = models.TextField(blank=True, help_text="key/name")
    # ~ annotation = models.ForeignKey(LibItemAnnotation, related_name="annotation_items",
    # ~ on_delete=models.CASCADE, null=True, blank=True, help_text="lib item annotations")
    tags = models.ManyToManyField(Tag, blank=True, help_text="tags")
    description = models.TextField(
        blank=True, null=True, help_text="LibItemDescription")

    def __str__(self):
        return self.caption or ""

    def __repr__(self):
        return self.caption or ""


class LibItemCollection(models.Model):
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    """ Collections can be used to group and organise LibItems"""
    libitem_collection_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="name of the collection")
    # parent -> llist
    libitems = models.ManyToManyField(
        LibItem, blank=True, help_text="libitems")
    date_added = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    date_modified = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    client_date_modified = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    # zotero key - might be redundand
    key = models.TextField(blank=True, help_text="key/name")

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""

# full text index words


class FulltextLibItemWords(models.Model):
    """ Collections can be used to group and organise LibItems"""
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    word_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    word = models.TextField(blank=True, null=True,
                            help_text="fulltext word of item")

    def __str__(self):
        return self.word or ""

    def __repr__(self):
        return self.word or ""


class FulltextLibItem(models.Model):
    """ Collections can be used to group and organise LibItems"""
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    fulltextitem_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    libitem = models.ForeignKey(LibItem, related_name="libitem_fulltextlibitems",
                                on_delete=models.CASCADE, null=True, blank=True, help_text="item that has been indexed")
    words = models.ManyToManyField(FulltextLibItemWords, related_name='%(app_label)s_%(class)s_words_related',
                                   related_query_name="%(app_label)s_%(class)s_words_related_query",
                                   blank=True, help_text="like maps to location etc.")
    indexed_pages = models.IntegerField(
        blank=True, null=True, help_text="total number of ")
    total_pages = models.IntegerField(
        blank=True, null=True, help_text="total number of ")
    indexed_chars = models.IntegerField(
        blank=True, null=True, help_text="total number of ")
    total_chars = models.IntegerField(
        blank=True, null=True, help_text="total number of ")
    version = models.IntegerField(
        blank=True, null=True, help_text="version of ??")
    synced = models.BooleanField(default=False)

    date_added = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    date_modified = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    client_date_modified = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")

    def __str__(self):
        return self.libitem.caption or ""

    def __repr__(self):
        return self.libitem.caption or ""

# base field mappings / custom base fields - purpose ?


class FeedItem(models.Model):
    """ A generic Reference """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    feeditem_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    caption = models.TextField(blank=True, help_text="generic text field")
    text = models.TextField(blank=True, help_text="generic text field")

    def __str__(self):
        return self.caption or ""

    def __repr__(self):
        return self.caption or ""


class Feed(models.Model):
    """ A generic Reference """
    #id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    feed_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="generic text field")
    URL = models.URLField(unique=True, blank=True, help_text="URL of feed")
    last_update = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    last_check = models.DateTimeField(
        default=timezone.now, help_text=" default: date time of now")
    lastCheckError = models.TextField(
        blank=True, null=True, help_text="generic text field")
    cleanupReadAfter = models.IntegerField(blank=True, null=True, help_text="")
    cleanupUnreadAfter = models.IntegerField(
        blank=True, null=True, help_text="")
    refreshInterval = models.IntegerField(blank=True, null=True, help_text="")

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""
